describe("Parser", function()
{
    var parser = new Parser();

    it("Entrada vacia o nula deberia devolver 0 resultados", function()
    {
        var resultados = parser.parsea();
        expect(0).toEqualLength(resultados);

        resultados = parser.parsea("");
        expect(0).toEqualLength(resultados);
    });

    it("Deberia haber un resultado por cada flag", function()
    {
        var resultados = parser.parsea("-a");
        expect(1).toEqualLength(resultados);

        var resultados = parser.parsea("-a -b");
        expect(2).toEqualLength(resultados);
    });

    it("Los resultados deberian ser los esperados en caso de flags simples", function()
    {
        var resultados = parser.parsea("-a");
        expect(resultados["a"]).toBeDefined();

        var resultados = parser.parsea("-a -b");
        expect(resultados["a"]).toBeDefined();
        expect(resultados["b"]).toBeDefined();
    });

    it("Los resultados deberian ser los esperados en caso de flags simples y compuestos",
            function()
            {
                var resultados = parser.parsea("-l -p 8080 -d /usr/logs");
                expect(resultados["l"]).toBeDefined();
                expect(resultados["p"]).toEqual(8080);
                expect(resultados["d"]).toEqual("/usr/logs");
            });

});

