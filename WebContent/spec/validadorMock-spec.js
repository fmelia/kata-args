describe("Validador Mock", function()
{
    var parser = new Parser();
    var validador = new Validador(parser);

    it("Deberia llamarse a la funcion de convertir del conversor", function()
    {
        spyOn(parser, "parsea").andCallThrough();

        validador.validar("", {});

        expect(parser.parsea).toHaveBeenCalled();
    });

    it("Deberia ser correcto si no pasamos nada y no esperamos nada", function()
    {
        spyOn(parser, "parsea").andReturn({});

        var resultado = validador.validar("valor moqueado", {});

        expect(resultado).toBe(true);
    });

    it("Deberia ser correcto si todos los argumentos estan en la lista", function()
    {
        spyOn(parser, "parsea").andReturn(
        {
            "a" : true,
            "b" : true
        });

        var resultado = validador.validar("valor mockeado",
        {
            "a" : 'boolean',
            "b" : 'boolean'
        });

        expect(resultado).toBe(true);
    });

    it("Deberia dar una excepcion si pasamos args que no esten en la lista", function()
    {
        spyOn(parser, "parsea").andReturn(
        {
            a : true
        });

        expect(function()
        {
            validador.validar("valor mockeado", {});
        }).toThrow();
    });

    it("Deberia ser correcto si los tipos son correctos", function()
    {
        spyOn(parser, "parsea").andReturn(
        {
            "l" : true,
            "p" : 8080,
            "d" : '/var/log'
        });

        var resultado = validador.validar("valor mockeado",
        {
            "l" : 'boolean',
            "p" : 'number',
            "d" : 'string',
        });

        expect(resultado).toBe(true);
    });

    it("Deberia dar una excepcion si alguno de los tipos no coincide", function()
    {
        spyOn(parser, "parsea").andReturn(
        {
            "l" : true,
            "p" : 8080,
            "d" : '/var/log'
        });

        expect(function()
        {
            validador.validar("valor mockeado",
            {
                "l" : 'string',
                "p" : 'string',
                "d" : 'string',
            });
        }).toThrow();
    });
});
