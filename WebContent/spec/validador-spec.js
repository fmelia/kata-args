describe("Validador", function()
{
    var parser = new Parser();
    var validador = new Validador(parser);

    it("Deberia ser correcto si no pasamos nada y no esperamos nada", function()
    {
        var resultado = validador.validar("", {});

        expect(resultado).toBe(true);
    });

    it("Deberia ser correcto si todos los argumentos estan en la lista", function()
    {
        var resultado = validador.validar("-a -b",
        {
            "a" : 'boolean',
            "b" : 'boolean'
        });

        expect(resultado).toBe(true);
    });

    it("Deberia dar una excepcion si pasamos args que no esten en la lista", function()
    {
        expect(function()
        {
            validador.validar("-a", {});
        }).toThrow();
    });

    it("Deberia ser correcto si los tipos son correctos", function()
    {
        var resultado = validador.validar("-l -p 8080 -d /var/log",
        {
            "l" : 'boolean',
            "p" : 'number',
            "d" : 'string',
        });

        expect(resultado).toBe(true);
    });

    it("Deberia dar una excepcion si alguno de los tipos no coincide", function()
    {
        expect(function()
        {
            validador.validar("-l -p 8080 -d /var/log",
            {
                "l" : 'boolean',
                "p" : 'string',
                "d" : 'string',
            });
        }).toThrow();
    });
});
