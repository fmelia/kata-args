function Validador(parser)
{
    this.validar = function(args, argsPosibles)
    {
        var argsConvertidos = parser.parsea(args);

        if (noHayArgumentosNiSeEsperan(argsConvertidos, argsPosibles))
        {
            return true;
        }
        
        return compruebaEquivalencia(argsConvertidos, argsPosibles);
    };

    function noHayArgumentosNiSeEsperan(argsConvertidos, argsPosibles)
    {
        return (length(argsConvertidos) == 0 && length(argsPosibles) == 0);
    }

    function compruebaEquivalencia(argsConvertidos, argsPosibles)
    {
        for (arg in argsConvertidos)
        {
            compruebaArgumentoEnLista(arg, argsPosibles);
            compruebaTipoArgumentoEnLista(arg, argsConvertidos, argsPosibles);
        }
        
        return true;
    }

    function compruebaArgumentoEnLista(arg, argsPosibles)
    {
        if (!argsPosibles[arg])
        {
            throw "algun args es erroneo";
        }
    }

    function compruebaTipoArgumentoEnLista(arg, argsConvertidos, argsPosibles)
    {
        var tipoEsperado = argsPosibles[arg];

        if (!(typeof argsConvertidos[arg] == tipoEsperado))
        {
            throw "tipo de algun arg es erroneo";
        }
    }
}