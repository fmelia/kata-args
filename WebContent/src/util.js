function length(diccionario)
{
    return Object.keys(diccionario).length;
}

function toNumber(valor)
{
    return new Number(valor).valueOf();
};

function borrarPrimerElemento(array)
{
    if (array.length > 0)
    {
        array.splice(0, 1);
    }

    return array;
};

beforeEach(function()
{
    this.addMatchers(
    {
        toEqualLength : function(diccionario)
        {
            return this.actual == length(diccionario);
        }
    });
});