function Parser()
{
    this.parsea = function(args)
    {
        var resultado = {};

        if (args)
        {
            resultado = convierteArgsEnDiccionario(args);
        }

        return resultado;
    };

    convierteArgsEnDiccionario = function(args)
    {
        var resultadosPorProcesar = convierteStringEnArray(args);

        var diccionario= {};

        for (indiceResultado in resultadosPorProcesar)
        {
            var resultadoPorProcesar = resultadosPorProcesar[indiceResultado];

            var claveResultado = dameClaveResultado(resultadoPorProcesar);
            var valorResultado = dameValorResultado(resultadoPorProcesar);

            diccionario[claveResultado] = valorResultado;
        }

        return diccionario;
    };

    dameClaveResultado = function(resultado)
    {
        resultado = resultado.trim();

        var resultadoSeparado = resultado.split(" ");

        return resultadoSeparado[0].trim();
    };

    dameValorResultado = function(resultado)
    {
        resultado = resultado.trim();

        var resultadoSeparado = resultado.split(" ");

        if (resultado[1])
        {
            var valor = resultadoSeparado[1].trim();

            if (!isNaN(valor))
            {
                return toNumber(valor);
            }

            return valor;
        }

        return true;
    };

    convierteStringEnArray = function(args)
    {
        var argsEnArray = args.split("-");
        argsEnArray = borrarPrimerElemento(argsEnArray);

        return argsEnArray;
    };
}